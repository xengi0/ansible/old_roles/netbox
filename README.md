Netbox
======

Setup netbox by DigitalOcean.

Requirements
------------

This role assumes a minimal CentOS 8 installation and will install all necessary dependencies.

Role Variables
--------------

### Role defaults

| Variable                    | Type        | Default  | Description |
| --------------------------- | ----------- | -------- | ----------- |
| `netbox__version`           | `str`       | `latest` | Can be set to a specific version to install, defaults to the latest upstream version. |
| `netbox__database_host`     | `str`       |          | FQDN or IP of the postgres database host that should be used instead of a local one. |
| `netbox__database_port`     | `str`       | `5432`   | Port of the postgres database host that should be used instead of a local one. |
| `netbox__database_name`     | `str`       | `netbox` | Name of the postgres database that will be created. |
| `netbox__database_username` | `str`       | `netbox` | Name of the postgres role that will be created. |
| `netbox__user`              | `str`       | `netbox` | Name of the system user that will be created. |
| `netbox__group`             | `str`       | `netbox` | Name of the system users group that will be created. |
| `netbox__allowed_hosts`     | `List[str]` | `['*']`  | List of FQDNs netbox will be accessable at, default to any hostname. |

*If `netbox__database_host` is left empty a local postgres database server will be installed and used. If a FQDN or IP
is given the installation will be skipped and the remote one will be used.*

### Role variables

| Variable           | Type        | Description |
| ------------------ | ----------- | ----------- |
| `netbox__packages` | `List[str]` | List of system dependencies. |

### Role parameters

| Variable                    | Type  | Description |
| --------------------------- | ----- | ----------- |
| `netbox__database_password` | `str` | Password that will be set for the postgres role. |
| `netbox__secret_key`        | `str` | String that should set as Django `SECRET_KEY`. **You need to escape single quotes.** |

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```
- hosts: servers
  roles:
    - role: xengi.netbox
      netbox__database_password: "{{ lookup('passwordstore', 'olympus/netbox.example.com/postgres/netbox') }}"
      netbox__secret_key: "{{ lookup('passwordstore', 'olympus/netbox.example.com/netbox subkey=secret_key') }}"
```

License
-------

MIT

Author Information
------------------

- Ricardo (XenGi) Band <email@ricardo.band>

